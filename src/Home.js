import React, { Component } from 'react';
import IngredientList from './IngredientList';
import './home.css';
class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
          recipes: JSON.parse(localStorage.getItem('recipes')) || []

        };

        this.displayRecipes = this.displayRecipes.bind(this);
    }

     displayRecipes() {
        let resArray = [];
        this.state.recipes.map((item, i) => {
             resArray.push(

                 <div  key={i} className="col-md-4 left-side">
                     {item.name}
                     <br/>
                     {item.description}

                     <IngredientList recipe={item} />
                 </div>
             );

         });
            return resArray;
         }


    render() {
        return(
            <div className="row">
                <h1>Home</h1>

                    { this.displayRecipes()}

            </div>
        );
    }

}

export default Home;
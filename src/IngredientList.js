import React, { Component } from 'react';

class IngredientList extends Component {
    displayIngredients() {
        let resArray =[];
        this.props.recipe.ingredients.map((item, i) => {
           resArray.push(
               <li key={i}>
                   {item.quantity} - {item.ingredient}
               </li>
           );
        });

        return resArray;
    }

    render() {
        return(
            <ul>
                {this.displayIngredients()}
            </ul>
        );
    }

}

export default IngredientList;
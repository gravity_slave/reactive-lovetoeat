import  React, { Component } from 'react';

class Ingredients extends Component {
    constructor(props) {
        super(props);

        this.state = {};
        this.addIngredients = this.addIngredients.bind(this);
    }

    addIngredients() {
        console.log("Ingredient added");
        console.log(this.ingredient.value, this.quantity.value);
        this.props.addIngredient(this.quantity.value, this.ingredient.value)
        this.quantity.value = null;
        this.ingredient.value = null;
    }

    render() {
        return (
            <div className="form-group form-inline">
                <label htmlFor="quantity">Quantity</label>
                <input type="text"
                       id="quantity"

                       ref={inputQuantity => this.quantity = inputQuantity }
                       className="form-control"
                       placeholder="Enter quantity" />
                <span> </span>
                <label htmlFor="ingredient">Ingredient</label>
                <input type="text"
                       id="ingredient"

                       ref={inputIngredient => this.ingredient = inputIngredient}
                       className="form-control"
                       placeholder="Enter ingredient" />
                <span> </span>
                <button type="button" className="btn btn-info" onClick={this.addIngredients}>Add</button>
            </div>
        );
    }
}

export default Ingredients;
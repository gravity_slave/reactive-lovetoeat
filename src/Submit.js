import React, { Component } from 'react';
import Ingredients from './Ingredients';
import IngredientList from  './IngredientList';
import { browserHistory } from 'react-router';
import Dropzone from 'react-dropzone';
import './submit.css';
import request from  'superagent';

const CLOUDINARY_UPLOAD_URL = 'https://api.cloudinary.com/v1_1/gravityslave/upload';

class Submit extends Component {
    constructor(props){
        super(props);

        this.state={
            recipes: JSON.parse(localStorage.getItem('recipes')) || [],
            newRecipe: {
                name: "New Recipe",
                description: "Description",
                ingredients: []
            },
            uploadedFileCloudinaryUrl: ''
        };
        this.onImageDrop = this.onImageDrop.bind(this);
        this.submitRecipe = this.submitRecipe.bind(this);
        this.handleImageUpload = this.handleImageUpload.bind(this);

    }

    submitRecipe() {
        console.log("Submit recipe");
        console.log(this.name.value, this.description.value);

        let { newRecipe } =  this.state;
        newRecipe.name = this.name.value;
        newRecipe.description = this.description.value;
        this.setState({newRecipe});
        console.log(newRecipe);

        let { recipes } =this.state;
        recipes.push(newRecipe);
        this.setState({ recipes });
        console.log(recipes);

        localStorage.setItem('recipes', JSON.stringify(recipes));
        browserHistory.push('/');
    }

        addIngredients(quantity, ingredient) {
        console.log("Add Ingredients in Submit js", quantity, ingredient);
        let newRecipe = this.state.newRecipe;
        newRecipe.ingredients.push({quantity, ingredient});
        this.setState({newRecipe});
        console.log(newRecipe);
    }

    onImageDrop(files) {

        this.setState({
            uploadedFile: files[0]
        });

        this.handleImageUpload(files[0]);
    }

    handleImageUpload(file) {
        let upload = request.post(CLOUDINARY_UPLOAD_URL)
            .field('file', file);

        upload.end((err, response) => {
            if (err) {
                console.error(err);
            }

            if (response.body.secure_url !== '') {
                this.setState({
                    uploadedFileCloudinaryUrl: response.body.secure_url
                });
            }
        });
    }

    render() {
        return(
            <div className="row">
                <div className="col-xs-12 col-sm-12">
                    <h1>Submit</h1>
                     </div>

                <form className="sit-down">

                    <Dropzone
                    multiple={false}
                    accept="image/*"
                    onDrop={this.onImageDrop}
                    >
                    <p> Drop an image or click  to  select a file to upload </p>
                    </Dropzone>

                    <div>
                        {this.state.uploadedFileCloudinaryUrl === '' ? null :
                            <div>
                                <p>{this.state.uploadedFile.name}</p>
                                <img src={this.state.uploadedFileCloudinaryUrl} alt={this.state.uploadedFileCloudinaryUrl.name} />
                            </div>}
                    </div>

                    <div className="form-group">
                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <input type="text"
                                   ref={ inputName => this.name = inputName }
                                   className="form-control"
                                   id="name"
                                   placeholder="Enter the name of the recipie" />
                        </div>

                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Password</label>
                        <textarea  className="form-control"
                                   ref={ inputDescription => this.description = inputDescription }
                                   id="description"
                                   placeholder="Enter the description of your recipe" />
                    </div>
                    <IngredientList recipe={this.state.newRecipe} />
                    <Ingredients addIngredient={(quantity, ingredient) => this.addIngredients(quantity,ingredient)} />


                    <button type="button" className="btn btn-default" onClick={this.submitRecipe}>Submit</button>
                </form>
            </div>
        );
    }

}

export  default Submit;